FROM maven:3-jdk-7-onbuild
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "target/hello-1.0-SNAPSHOT.jar"]
